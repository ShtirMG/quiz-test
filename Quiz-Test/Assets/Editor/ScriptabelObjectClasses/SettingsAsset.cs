﻿using UnityEngine;
using UnityEditor;

namespace Quiz.Helpers
{
    public class SettingsAsset
    {
        [MenuItem("Assets/Create/QuizSettings")]
        public static void CreateQuizSettingsAsset()
        {
            ScriptableSaveHelper.CreateAsset<SettingsObject>();
        }
    }
}