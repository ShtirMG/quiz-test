﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Quiz
{
    public class EndGamePanel : MonoBehaviour
    {
        [SerializeField] Text endGameText;
        [SerializeField] Button continueButton;
        [SerializeField] Button restartButton;

        /// <summary>
        /// Sets panel when player win
        /// </summary>
        /// <param name="score">Player score</param>
        public void SetWinPanel(int score)
        {
            endGameText.text = "YOU WIN! YOUR SCORE: " + score;
            continueButton.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(false);
        }

        /// <summary>
        /// Sets panel when player win
        /// </summary>
        /// <param name="isWordsLeft">Is any words left to guess</param>
        public void SetLoosePanel (bool isWordsLeft)
        {
            endGameText.text = "YOU LOSE AND LOST ALL SCORES!";
            continueButton.gameObject.SetActive(isWordsLeft);
            restartButton.gameObject.SetActive(!isWordsLeft);
        }

        /// <summary>
        /// Sets panel when player beats the game
        /// </summary>
        /// <param name="score">Player score</param>
        public void SetRestartPanel(int score)
        {
            endGameText.text = "CONGRATULATIONS! YOU BEAT THE GAME! YOUR FINAL SCORE: " + score;
            continueButton.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(true);
        }

        /// <summary>
        /// Sets panel when there is no words in quiz controller (due to empty source text or too long minWordLength setting)
        /// </summary>
        public void SetErrorPanel()
        {
            endGameText.text = "Something went wrong - there is no words to guess!";
            continueButton.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(false);
        }
    }
}