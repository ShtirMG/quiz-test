﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Quiz
{
    public class LetterButton : MonoBehaviour
    {
        [SerializeField] GameObject pressedHolder;
        [SerializeField] Button button;
        public string Letter { get; private set; }
        
        public void Init(string letter, GameManager manager)
        {
            Text text = GetComponentInChildren<Text>();
            text.text = letter;
            Letter = letter;
            button.onClick.AddListener(() => manager.OnKeyboardPress(this));
        }
        
        public void OnLetterKeyPressed()
        {
            pressedHolder.SetActive(true);
            button.gameObject.SetActive(false);
        }
        
        public void ResetButton()
        {
            pressedHolder.SetActive(false);
            button.gameObject.SetActive(true);
        }

        private void OnDestroy()
        {
            button.onClick.RemoveAllListeners();
        }
    }
}