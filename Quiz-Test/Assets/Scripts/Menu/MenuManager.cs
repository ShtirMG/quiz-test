﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Quiz.Menu
{
    public class MenuManager : MonoBehaviour
    {
        [SerializeField] InputField minWordLengthInput;
        [SerializeField] InputField maxPlayerErrorsInput;
        [SerializeField] Dropdown gameModeDropDown;

        [SerializeField] GameObject menuPanel;
        [SerializeField] GameObject settingsPanel;

        private void Start()
        {
            minWordLengthInput.text = GameSettings.Instance.Settings.WordMinLength.ToString();
            maxPlayerErrorsInput.text = GameSettings.Instance.Settings.PlayerErrorAmount.ToString();
            gameModeDropDown.value = (int) GameSettings.Instance.Settings.GameMode;
        }

        /// <summary>
        /// When Player clicks on "Play" button
        /// </summary>
        public void OnPlayClick()
        {
            SceneManager.LoadSceneAsync("Game");
        }

        /// <summary>
        /// When player clicks on "Settings" button
        /// </summary>
        public void OnSettingsClick()
        {
            menuPanel.SetActive(false);
            settingsPanel.SetActive(true);
        }

        /// <summary>
        /// When Player clicks on "Back" button in settings panel
        /// </summary>
        public void OnBackClick()
        {
            menuPanel.SetActive(true);
            settingsPanel.SetActive(false);
        }
        
        /// <summary>
        /// When player clicks on "Exit" button
        /// </summary>
        public void OnExitClick()
        {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit ();
        #endif
        }

        /// <summary>
        /// When player clicks on "Apply" button in settings panel
        /// </summary>
        public void OnApplySettingsClick()
        {
            int newMinWordLength = Convert.ToInt32(minWordLengthInput.text);
            int newPlayerErrorsCount = Convert.ToInt32(maxPlayerErrorsInput.text);
            GameType newGameMode = (GameType)gameModeDropDown.value;

            GameSettings.Instance.Settings.SetSettingsValues(newMinWordLength, newPlayerErrorsCount, newGameMode);
        }
    }
}