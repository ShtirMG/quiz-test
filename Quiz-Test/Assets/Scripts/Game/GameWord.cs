﻿using UnityEngine;

namespace Quiz
{
    /// <summary>
    /// Class of word used in game. Parameter "Using" represents how many times word meets in text
    /// </summary>
    public class GameWord
    {
        public string Text { get; private set; }
        public int Using { get; private set; }

        public GameWord(string word)
        {
            Text = word;
            Using = 1;
        }

        /// <summary>
        /// Increase Using by 1 (if word meets in text again)
        /// </summary>
        public void IncreaseUsing()
        {
            Using+=1;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GameWord))
            {
                return false;
            }

            GameWord gameWord = (GameWord)obj;

            return this.Text == gameWord.Text;
        }

        public static bool operator ==(GameWord a, GameWord b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(GameWord a, GameWord b)
        {
            return !a.Equals(b);
        }

        public override int GetHashCode()
        {
            return Text.GetHashCode();
        }
    }
}