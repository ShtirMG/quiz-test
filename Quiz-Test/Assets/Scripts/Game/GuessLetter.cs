﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Quiz
{
    public class GuessLetter : MonoBehaviour
    {
        [SerializeField] Text textComponent;
        
        public void SetLetter(char letter)
        {
            textComponent.text = letter.ToString();
        }
    }
}