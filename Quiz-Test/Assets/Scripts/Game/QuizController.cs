﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Quiz
{
    /// <summary>
    /// Class with quiz in-game logic
    /// </summary>
    public class QuizController
    {
        public List<GameWord> AvailibleWords { get; private set; }

        private GameWord[] allWords; //array to reset the game
        private int wordsCount;

        /// <summary>
        /// Default constructor of QuizController
        /// </summary>
        /// <param name="sourceText">Original text source</param>
        /// <param name="minWordLength">Minimal word length. Words shorter than minWordLength will be ignored</param>
        public QuizController(string sourceText, int minWordLength)
        {
            AvailibleWords = new List<GameWord>();
            Regex regex = new Regex("[^a-zA-Z ]");
            sourceText = regex.Replace(sourceText, " ");
            sourceText = sourceText.ToUpper();
            regex = new Regex(@"\s*\b\w{1," + minWordLength + @"}\b\s*");
            sourceText = regex.Replace(sourceText, " ");
            sourceText = sourceText.Trim();
            string[] words = sourceText.Split(' ').Where(x => x.Length > 0).ToArray();
            wordsCount = words.Length;


            foreach (string word in words)
            {
                if (!AvailibleWords.Any(x => x.Text == word))
                {
                    AvailibleWords.Add(new GameWord(word));
                }
                else
                {
                    IncreaseWordUsing(word);
                }
            }
            allWords = new GameWord[AvailibleWords.Count];
            AvailibleWords.CopyTo(allWords);
            if (AvailibleWords.Count == 0)
            {
                Debug.LogWarning("There is no words in source or no words with such minimal length");
            }
        }
        
        /// <summary>
        /// Takes and deletes from list of availible words random word based on its frequency in source text
        /// </summary>
        /// <returns>Word in string</returns>
        public string TakeRandomMostUsedWord()
        {
            if (AvailibleWords.Count > 0)
            {
                int rndWord = Random.Range(1, wordsCount);
                int currentSum = 1;
                GameWord resultWord = null;
                Debug.Log("RND = " + rndWord);
                foreach (GameWord gameWord in AvailibleWords)
                {
                    if (currentSum <= rndWord && rndWord < currentSum + gameWord.Using)
                    {
                        resultWord = gameWord;
                        break;
                    }
                    currentSum += gameWord.Using;
                }
                AvailibleWords.Remove(resultWord);
                wordsCount -= resultWord.Using;
                return resultWord.Text;
            }
            
            return null;
        }

        /// <summary>
        /// Takes and deletes from list of availible words random word based on its reverse frequency in source text
        /// </summary>
        /// <returns>Word in string</returns>
        public string TakeRandomLessUsedWord()
        {
            if (AvailibleWords.Count > 0)
            {
                int rndWord = Random.Range(1, wordsCount * AvailibleWords.Count);
                int currentSum = 1;
                GameWord resultWord = null;
                foreach (GameWord gameWord in AvailibleWords)
                {
                    if (currentSum <= rndWord && rndWord < currentSum + ((wordsCount - gameWord.Using)))
                    {
                        Debug.Log(wordsCount);
                        resultWord = gameWord;
                        break;
                    }
                    currentSum += (wordsCount - gameWord.Using);
                }
                AvailibleWords.Remove(resultWord);
                wordsCount -= resultWord.Using;
                Debug.Log("Selected word is " + resultWord.Text + " (used " + resultWord.Using + " times)");
                return resultWord.Text;
            }

            return null;
        }

        /// <summary>
        /// Takes and deletes from list of availible words random word
        /// </summary>
        /// <returns>Word in string</returns>
        public string TakeRandomNormalWord()
        {
            if (AvailibleWords.Count > 0)
            {
                GameWord resultWord = AvailibleWords[Random.Range(0, AvailibleWords.Count)];
                AvailibleWords.Remove(resultWord);
                return resultWord.Text;
            }

            return null;
        }

        /// <summary>
        /// Reset availible words
        /// </summary>
        public void Reset()
        {
            AvailibleWords = new List<GameWord>();
            AvailibleWords.AddRange(allWords);
        }

        private void IncreaseWordUsing(string word)
        {
            GameWord gw = AvailibleWords.Where(x => x.Text == word).SingleOrDefault();
            if (gw != null)
            {
                gw.IncreaseUsing();
            }
        }

    }
}