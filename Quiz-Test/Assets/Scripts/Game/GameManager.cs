﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Quiz
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] GameObject keyboardPanel;
        [SerializeField] Canvas mainCanvas;
        [SerializeField] LetterButton keyButtonPrefab;
        [SerializeField] GuessLetter guessLetterPrefab;
        [SerializeField] Text scoreField;
        [SerializeField] Text errorField;
        [SerializeField] GameObject loadingPanel;
        [SerializeField] EndGamePanel endGamePanel;
        [SerializeField] Transform guessLettersParent;

        private List<GuessLetter> letterToGuessList;
        private int errorsLeft;
        private int lettersLeftToGuess;
        private int playerScore = 0;
        private QuizController quizController;

        private string wordToGuess;

        private void Awake()
        {
            loadingPanel.SetActive(true);
            letterToGuessList = new List<GuessLetter>();
            InitializeKeyboard();
            StartCoroutine(LoadBundle());
            playerScore = 0;
        }

        /// <summary>
        /// Async load of asset bundle
        /// </summary>
        /// <returns></returns>
        IEnumerator LoadBundle()
        {
            string sourceText;
            TextAsset textAsset;
            AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, "quiztext"));
            yield return request;

            if (request.assetBundle == null)
            {
                Debug.LogError("Can't load game assetbundle!");
                yield break;
            }

            AssetBundleRequest abr = request.assetBundle.LoadAssetAsync<TextAsset>("alice30.txt");

            yield return abr;

            if (abr == null)
            {
                Debug.Log("Can't load game text!");
                yield break;
            }

            textAsset = abr.asset as TextAsset;
            sourceText = textAsset.text;
            
            request.assetBundle.Unload(false);
            quizController = new QuizController(sourceText, GameSettings.Instance.Settings.WordMinLength);
            StartNewGame();
            if (loadingPanel != null)
            {
                loadingPanel.SetActive(false);
            }
        }
        
        /// <summary>
        /// Init virtual keyboard
        /// </summary>
        private void InitializeKeyboard()
        {
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Transform keyboardTransform = keyboardPanel.transform;
            RectTransform keyboardRT = keyboardPanel.GetComponent<RectTransform>();
            float keyboardWidth = keyboardRT.rect.width;
            float keyCoordX = -20;
            float keyCoordY = -30;
            foreach (char letter in alphabet)
            {
                if (keyCoordX + 50 > keyboardWidth - 30)
                {
                    keyCoordX = 30;
                    keyCoordY = keyCoordY - 50;
                }
                else
                {
                    keyCoordX += 50;
                }

                LetterButton letterButton = Instantiate(keyButtonPrefab);
                letterButton.Init(letter.ToString(), this);
                RectTransform letterButtonRT = letterButton.GetComponent<RectTransform>();

                letterButtonRT.SetParent(keyboardTransform);
                letterButtonRT.anchoredPosition = new Vector3(keyCoordX, keyCoordY, 0);
            }
        }
        
        /// <summary>
        /// Starts new game with random word (except already used)
        /// </summary>
        public void StartNewGame()
        {
            
            wordToGuess = TakeRandomWord();

            if (!string.IsNullOrEmpty(wordToGuess))
            {
                endGamePanel.gameObject.SetActive(false);
                lettersLeftToGuess = wordToGuess.Length;
                errorsLeft = GameSettings.Instance.Settings.PlayerErrorAmount;

                LetterButton[] letterButtons = keyboardPanel.GetComponentsInChildren<LetterButton>();
                foreach (LetterButton letterButton in letterButtons)
                {
                    letterButton.ResetButton();
                }

                if (letterToGuessList.Count > 0)
                {
                    foreach (GuessLetter gl in letterToGuessList)
                    {
                        GameObject.Destroy(gl.gameObject);
                    }
                    letterToGuessList = new List<GuessLetter>();
                }

                float xPos = 75 / 2 - (75 * wordToGuess.Length / 2);

                for (int i = 0; i < wordToGuess.Length; i++)
                {
                    GuessLetter guessLetter = Instantiate<GuessLetter>(guessLetterPrefab);
                    RectTransform guessLetterRT = guessLetter.GetComponent<RectTransform>();
                    guessLetterRT.SetParent(guessLettersParent);
                    guessLetterRT.anchoredPosition = new Vector2(xPos + i * 75, 50);
                    letterToGuessList.Add(guessLetter);
                }
                RefreshGUIText();
            }
            else
            {
                endGamePanel.SetErrorPanel();
                endGamePanel.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Fully restarts game, drops scores and refreshes availble words for play
        /// </summary>
        public void RestartGame()
        {
            playerScore = 0;
            quizController.Reset();
            StartNewGame();
        }

        /// <summary>
        /// When player clicks on virtual keyboard
        /// </summary>
        /// <param name="letterButton">Clicked button</param>
        public void OnKeyboardPress(LetterButton letterButton)
        {
            if (wordToGuess.Contains(letterButton.Letter))
            {
                for(int i=0; i<wordToGuess.Length; i++)
                {
                    if (letterButton.Letter == wordToGuess[i].ToString())
                    {
                        letterToGuessList[i].SetLetter(wordToGuess[i]);
                        lettersLeftToGuess--;
                    }
                }
            }
            else
            {
                errorsLeft--;
                RefreshGUIText();
            }

            letterButton.OnLetterKeyPressed();

            if (errorsLeft <= 0)
            {
                EndGame(false);
            }
            else
            {
                if (lettersLeftToGuess <= 0)
                {
                    EndGame(true);
                }
            }
        }
        
        /// <summary>
        /// Manages end game screen
        /// </summary>
        /// <param name="isPlayerWin">True if player wins</param>
        private void EndGame(bool isPlayerWin)
        {
            if (!isPlayerWin)
            {
                playerScore = 0;
                endGamePanel.SetLoosePanel(quizController.AvailibleWords.Count > 0);

            }
            else
            {
                playerScore += errorsLeft;
                if (quizController.AvailibleWords.Count > 0)
                {
                    endGamePanel.SetWinPanel(playerScore);
                }
                else
                {
                    endGamePanel.SetRestartPanel(playerScore);
                }
            }

            endGamePanel.gameObject.SetActive(true);

            RefreshGUIText();
        }

        /// <summary>
        /// When player clicks "Exit" button
        /// </summary>
        public void ReturnToMenu()
        {
            SceneManager.LoadScene("Menu");
        }

        /// <summary>
        /// Take random word for current game mode
        /// </summary>
        /// <returns>word in string</returns>
        private string TakeRandomWord()
        {
            switch(GameSettings.Instance.Settings.GameMode)
            {
                case GameType.MostUsed: return quizController.TakeRandomMostUsedWord();
                case GameType.Rare: return quizController.TakeRandomLessUsedWord();
                default: return quizController.TakeRandomNormalWord();
            }
        }

        /// <summary>
        /// Refreshes scores and error fields
        /// </summary>
        private void RefreshGUIText()
        {
            scoreField.text = "YOUR SCORE: " + playerScore;
            errorField.text = "ERRORS LEFT: " + errorsLeft;
        }
    }
}