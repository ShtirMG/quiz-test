﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Quiz
{
    /// <summary>
    /// Game settings script for scene object
    /// </summary>
    public class GameSettings : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("SettingsObject asset")]
        SettingsObject settings;

        public static GameSettings Instance { get; private set; }
        
        public SettingsObject Settings { get { return settings; } }

        private void Awake() //simple (and not perfect) singleton implementation
        {
            DontDestroyOnLoad(gameObject);
            if (GameSettings.Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}