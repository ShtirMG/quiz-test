﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Quiz
{
    public class SettingsObject : ScriptableObject
    {
        [SerializeField]
        [Tooltip("Minimal length of words used in game (greater than 0)")]
        private int wordMinLength = 3;
        [SerializeField]
        [Tooltip("Amount of errors that player can make in one session (greater or equal 0)")]
        private int playerErrorAmount = 10;

        [SerializeField]
        [Tooltip("Current Game Mode")]
        private GameType gameMode;
        /// <summary>
        /// Minimal length of words used in game
        /// </summary>
        public int WordMinLength { get { return wordMinLength; } }

        /// <summary>
        /// Amount of errors that player can make in one session
        /// </summary>
        public int PlayerErrorAmount { get { return playerErrorAmount; } }


        /// <summary>
        /// Current Game Mode
        /// </summary>
        public GameType GameMode { get { return gameMode; } }

        [ExecuteInEditMode]
        private void OnValidate() //validate when change in editor
        {
            if (wordMinLength < 1)
            {
                wordMinLength = 1;
            }

            if (playerErrorAmount < 0)
            {
                playerErrorAmount = 0;
            }
        }

        /// <summary>
        /// Set new setting values
        /// </summary>
        /// <param name="wordLength">Minimal length of words used in game</param>
        /// <param name="playerErrors">Amount of errors that player can make in one session</param>
        /// <param name="gamemode">Current Game Mode</param>
        public void SetSettingsValues(int wordLength, int playerErrors, GameType gamemode)
        {
            wordMinLength = wordLength;
            playerErrorAmount = playerErrors;
            gameMode = gamemode;

            OnValidate();
        }
    }

    public enum GameType
    {
        Normal = 0,
        Rare = 1,
        MostUsed = 2
    }
}